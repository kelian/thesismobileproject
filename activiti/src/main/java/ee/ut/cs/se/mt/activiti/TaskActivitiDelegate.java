package ee.ut.cs.se.mt.activiti;

import android.content.Context;
import android.content.Intent;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class TaskActivitiDelegate implements JavaDelegate {

    protected Class activityClass;

    protected void setActivityClass() { }

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        setActivityClass();

        ActivitiApp.androidActivityIsInProgress();

        Context appContext = ActivitiApp.getAppContext();
        appContext.startActivity(new Intent(appContext, activityClass).setFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK
        ));

        while (ActivitiApp.isAndroidActivityInProgress())
            Thread.sleep(1);
    }
}
