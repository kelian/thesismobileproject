package ee.ut.cs.se.mt.activiti.network;

import ee.ut.cs.se.mt.activiti.model.Plan;
import ee.ut.cs.se.mt.activiti.model.PlanRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface WebServiceEndPoint {

    String ACCEPT_JSON = "Accept: application/json";

    @POST("plan")
    Call<Plan> generatePlan(@Body PlanRequest planRequest);
}
