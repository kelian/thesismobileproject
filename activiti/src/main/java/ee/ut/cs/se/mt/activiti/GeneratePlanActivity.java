package ee.ut.cs.se.mt.activiti;

import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import ee.ut.cs.se.mt.activiti.model.Plan;
import ee.ut.cs.se.mt.activiti.model.PlanRequest;
import ee.ut.cs.se.mt.activiti.network.NoNetworkException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeneratePlanActivity extends AppCompatActivity {

    public interface NetworkListener {
        void onGeneratePlanSuccess(Plan plan);

        void onGeneratePlanFailure();
    }

    protected void generatePlan(final View view, PlanRequest planRequest, final NetworkListener listener) {
        ActivitiApp.getNetworkClient().generatePlan(planRequest, new Callback<Plan>() {
            @Override
            public void onResponse(Call<Plan> call, Response<Plan> response) {
                if (response.isSuccessful() && response.body() != null) {
                    listener.onGeneratePlanSuccess(response.body());
                } else {
                    onFailure();
                }
            }

            @Override
            public void onFailure(Call<Plan> call, Throwable t) {
                if (NoNetworkException.NO_NETWORK_CONNECTION.equals(t.getMessage())) {
                    listener.onGeneratePlanFailure();
                    Snackbar.make(view, "Check your network connection", Snackbar.LENGTH_LONG).show();
                } else {
                    onFailure();
                }
            }

            private void onFailure() {
                listener.onGeneratePlanFailure();
                Snackbar.make(view, "Generating plan failed", Snackbar.LENGTH_LONG).show();
            }
        });
    }
}
