package ee.ut.cs.se.mt.activiti;

import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class TaskActivitiActivity extends AppCompatActivity {

    protected View.OnClickListener finishListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ActivitiApp.androidActivityIsFinished();
            finish();
        }
    };

    @Override
    public void onBackPressed() {
        // Do nothing
    }
}
