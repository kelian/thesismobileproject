package ee.ut.cs.se.mt.activiti;

import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import org.activiti.engine.repository.ProcessDefinition;

import java.util.ArrayList;
import java.util.List;

import ee.ut.cs.mc.and.activiti521.HandlerCallback;
import ee.ut.cs.mc.and.activiti521.engine.ActivitiServiceManager;
import ee.ut.cs.mc.and.activiti521.engine.EngineStatusDescriber;
import ee.ut.cs.se.mt.activiti.model.Plan;

public class AppActivitiActivity extends AppCompatActivity {

    protected interface EngineStatusListener {
        void isProcessDeployed(ProcessDefinition process);
    }

    private ActivitiServiceManager activitiManager;
    private EngineStatusListener listener;

    private ArrayList<String> deployedProcesses = new ArrayList<>();

    private String appName;
    private String deployedResourceName;

    protected void initEngine(EngineStatusListener listener, String appName) {
        this.listener = listener;
        this.appName = appName;
        startEngine();
    }

    private void startEngine() {
        activitiManager = new ActivitiServiceManager(this);
        activitiManager.startService();
        activitiManager.bindToService();
        initEngineStatusListener(listener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (activitiManager != null) {
            activitiManager.unbindFromService();
        }
    }

    private void initEngineStatusListener(final EngineStatusListener listener) {
        final Handler handler = new Handler();

        final HandlerCallback<EngineStatusDescriber> engineStatsCallback =
                new HandlerCallback<EngineStatusDescriber>(handler) {
                    @Override
                    protected void handle(EngineStatusDescriber stats) {
                        saveProcessDefinitions(stats.processDefinitions);
                        listener.isProcessDeployed(getLatestDeployedProcess(stats.processDefinitions));
                    }
                };

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (activitiManager.isBound()) {
                    activitiManager.getService().getEngineStatus(engineStatsCallback);
                }
                handler.postDelayed(this, 3000);
            }
        }, 1000);
    }

    protected void deployProcess(Plan plan) {
        if (!deployedProcesses.contains(plan.getBpmnResourceName())) {
            activitiManager.getService().deployProcess(plan);
        }

        this.deployedResourceName = plan.getBpmnResourceName();
    }

    protected void startProcess(String processKey) {
        activitiManager.getService().startProcess(processKey);
    }

    private void saveProcessDefinitions(List<ProcessDefinition> processes) {
        deployedProcesses.clear();
        for (ProcessDefinition process : processes) {
            deployedProcesses.add(process.getResourceName());
        }
    }

    private ProcessDefinition getLatestDeployedProcess(List<ProcessDefinition> processes) {
        if (!processes.isEmpty()) {
            for (ProcessDefinition process : processes) {
                if (process.getResourceName().equals(deployedResourceName)) {
                    return process;
                }
            }

            for (ProcessDefinition process : processes) {
                if (process.getResourceName().startsWith(appName)) {
                    return process;
                }
            }
        }
        return null;
    }
}
