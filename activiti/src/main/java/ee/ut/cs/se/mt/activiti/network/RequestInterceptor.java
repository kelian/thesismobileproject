package ee.ut.cs.se.mt.activiti.network;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import ee.ut.cs.se.mt.activiti.ActivitiApp;
import ee.ut.cs.se.mt.activiti.Utils;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestInterceptor implements Interceptor {

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        if (!Utils.isConnectedToNetwork(ActivitiApp.getInstance())) {
            throw new NoNetworkException(NoNetworkException.NO_NETWORK_CONNECTION);
        }

        Request.Builder requestBuilder = chain.request().newBuilder();
        return chain.proceed(requestBuilder.build());
    }
}
