package ee.ut.cs.se.mt.activiti;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import ee.ut.cs.se.mt.activiti.network.WebServiceClient;

public class ActivitiApp extends Application {

    private static ActivitiApp instance;
    private static Boolean androidActivityInProgress = false;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static Context getInstance() {
        return instance;
    }

    public static Context getAppContext() {
        return instance.getApplicationContext();
    }

    public static void androidActivityIsInProgress() {
        androidActivityInProgress = true;
    }

    public static void androidActivityIsFinished() {
        androidActivityInProgress = false;
    }

    public static Boolean isAndroidActivityInProgress() {
        return androidActivityInProgress;
    }

    public static WebServiceClient getNetworkClient() {
        return WebServiceClient.getApiInstance();
    }

}
