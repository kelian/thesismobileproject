package ee.ut.cs.se.mt.activiti.model;

import java.io.Serializable;

public class Plan implements Serializable {

    private String bpmnResourceName;
    private String bpmnFile;

    Plan(String bpmnResourceName, String bpmnFile) {
        this.bpmnResourceName = bpmnResourceName;
        this.bpmnFile = bpmnFile;
    }

    public String getBpmnFile() {
        return bpmnFile;
    }

    public void setBpmnFile(String bpmnFile) {
        this.bpmnFile = bpmnFile;
    }

    public String getBpmnResourceName() {
        return bpmnResourceName;
    }

    public void setBpmnResourceName(String bpmnResourceName) {
        this.bpmnResourceName = bpmnResourceName;
    }
}
