package ee.ut.cs.se.mt.activiti;

import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import org.activiti.engine.runtime.ProcessInstance;

import java.util.List;

import ee.ut.cs.mc.and.activiti521.HandlerCallback;
import ee.ut.cs.mc.and.activiti521.engine.ActivitiServiceManager;
import ee.ut.cs.mc.and.activiti521.engine.EngineStatusDescriber;

public class MainActivitiActivity extends AppCompatActivity {

    protected interface EngineStatusListener {
        void isProcessRunning(List<ProcessInstance> processes);
    }

    private ActivitiServiceManager activitiManager;
    private EngineStatusListener listener;

    protected void initEngine(EngineStatusListener listener) {
        this.listener = listener;
        startEngine();
    }

    private void startEngine() {
        activitiManager = new ActivitiServiceManager(this);
        activitiManager.startService();
        activitiManager.bindToService();
        initEngineStatusListener(listener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (activitiManager != null) {
            activitiManager.unbindFromService();
        }
    }

    private void initEngineStatusListener(final EngineStatusListener listener) {
        final Handler handler = new Handler();

        final HandlerCallback<EngineStatusDescriber> engineStatsCallback =
                new HandlerCallback<EngineStatusDescriber>(handler) {
                    @Override
                    protected void handle(EngineStatusDescriber stats) {
                        listener.isProcessRunning(stats.runningInstances);
                    }
                };

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (activitiManager.isBound()) {
                    activitiManager.getService().getEngineStatus(engineStatsCallback);
                }
                handler.postDelayed(this, 3000);
            }
        }, 1000);
    }

    protected void continueProcess(String processInstanceId) {
        activitiManager.getService().continueProcess(processInstanceId);
    }
}
