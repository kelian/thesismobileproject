package ee.ut.cs.se.mt.activiti.network;

import java.io.IOException;

public class NoNetworkException extends IOException {

    public static final String NO_NETWORK_CONNECTION = "no_network_connection";

    NoNetworkException(String message) {
        super(message);
    }
}
