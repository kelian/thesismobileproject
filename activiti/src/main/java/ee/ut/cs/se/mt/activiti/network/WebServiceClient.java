package ee.ut.cs.se.mt.activiti.network;

import java.util.concurrent.TimeUnit;

import ee.ut.cs.se.mt.activiti.model.Plan;
import ee.ut.cs.se.mt.activiti.model.PlanRequest;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WebServiceClient {

    private static WebServiceClient webServiceClient = null;
    private WebServiceEndPoint webAPI;

    private final String ipAddress = ""; // todo - ADD IP ADDRESS
    private final String baseUrl = "http://" + ipAddress + ":8080";

    public synchronized static WebServiceClient getApiInstance() {
        if (webServiceClient == null) {
            webServiceClient = new WebServiceClient();
        }
        return webServiceClient;
    }

    private WebServiceClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        okHttpClientBuilder.addInterceptor(new RequestInterceptor());
        okHttpClientBuilder.readTimeout(60, TimeUnit.SECONDS);
        okHttpClientBuilder.connectTimeout(60, TimeUnit.SECONDS);

        addLoggingInterceptor(okHttpClientBuilder);

        OkHttpClient client = okHttpClientBuilder.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        webAPI = retrofit.create(WebServiceEndPoint.class);
    }

    private static void addLoggingInterceptor(OkHttpClient.Builder okHttpClientBuilder) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.level(HttpLoggingInterceptor.Level.BODY);
        okHttpClientBuilder.addInterceptor(logging);
    }

    // API Requests
    public void generatePlan(PlanRequest planRequest, Callback<Plan> callback) {
        webAPI.generatePlan(planRequest).enqueue(callback);
    }
}
