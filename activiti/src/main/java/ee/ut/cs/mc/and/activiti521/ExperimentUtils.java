package ee.ut.cs.mc.and.activiti521;

import android.util.Log;
import android.util.TimingLogger;

import org.activiti.engine.delegate.event.ActivitiActivityEvent;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import ee.ut.cs.mc.and.activiti521.engine.migration.MigrationListener;

/**
 * This class is meant to be used for logging various
 * activities as part of experiments
 * and their performance.
 */
public class ExperimentUtils {

    private static final String TAG = ExperimentUtils.class.getSimpleName();
    private static final int NO_OF_INSTANCES_TO_RUN = 1;

    public static TimingLogger timings;

    public static void experimentLog(String msg){
        Log.i(TAG, "[WW] "+msg);
    }

    // Immigration (loading process from outside)
    public static final String IMMIGRATION_PROC_INST_ID = "4";

    public static final boolean DELETE_FILES_ON_BOOT = false;

    // Emigration (taking process from Engine and serializing it to file
    public static final boolean AUTO_EMIGRATE = false;
    public static final int STEPS_BEFORE_EMIGRATION = NO_OF_INSTANCES_TO_RUN * 5; //How many tasks to finish before triggering emigration
    //5 emigrates after parallel join

    public static List<String> emigratingProcessInstanceList = new ArrayList<>();

    public static void finishedMigration(String processInstanceId) {
        finishedMigration(new String[]{processInstanceId});
    }

    public static void startingMigration(ActivitiActivityEvent event) {
        experimentLog("Migration start");
        if (MigrationListener.counter == STEPS_BEFORE_EMIGRATION) timings.reset();
        ExperimentUtils.timings.addSplit("Migration start");
    }

    public static void finishedMigration(String[] processInstanceIds) {
        experimentLog("Finished Migration");
        timings.addSplit("Finished Migration"+ StringUtils.join(processInstanceIds, ";"));
        timings.dumpToLog();
    }

    /** Get a list of running BP instance id's */
    public static String[] getListOfBPsToMigrate(ActivitiActivityEvent event) {
        List<ProcessInstance> instanceList = event.getEngineServices().getRuntimeService()
                .createProcessInstanceQuery().list();

        String[] instanceArray = new String[instanceList.size()];
        for (int i = 0; i < instanceList.size(); i++) {
            instanceArray[i] = instanceList.get(i).getProcessInstanceId();
        }

        return instanceArray;
    }
}
