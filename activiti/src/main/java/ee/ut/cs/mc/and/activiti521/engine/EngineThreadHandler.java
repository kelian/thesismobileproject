package ee.ut.cs.mc.and.activiti521.engine;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import ee.ut.cs.se.mt.activiti.model.Plan;

/**
 * Created by J-Kool on 29/09/2016.
 */
public class EngineThreadHandler extends Handler {
    public static final int ENGINE_THREAD_MSG_DEPLOY_PROCESS = 1;
    public static final int ENGINE_THREAD_MSG_RUN_PROCESS = 2;
    public static final int ENGINE_THREAD_MSG_CONTINUE_PROCESS = 6;
    public static final int ENGINE_THREAD_MSG_DELETE_PROCESS = 7;
    public static final int ENGINE_CAPTURE_INSTANCE_STATE = 3;
    public static final int ENGINE_LOAD_STATE_TO_DB = 4;
    public static final int EMIGRATE_PROCESS_INSTANCES = 5;
    public static final int ENGINE_THREAD_MSG_FINISH_TASK = 8;
    public static final int ENGINE_THREAD_MSG_CONTINUE_TASK = 9;

    private final EngineThread mEngineThread;

    public EngineThreadHandler(Looper looper, EngineThread engineThread) {
        super(looper);
        mEngineThread = engineThread;
    }
    public void handleMessage(Message msg) {
        if (!mEngineThread.isEngineInitialized()) return;
        int command = msg.what;

        switch (command){
            case ENGINE_THREAD_MSG_DEPLOY_PROCESS:
                Plan plan = (Plan) msg.obj;
                mEngineThread.deployProcess(plan);
                break;
            case ENGINE_THREAD_MSG_RUN_PROCESS:
                String processKey = (String) msg.obj;
                mEngineThread.startProcess(processKey);
                break;
            case ENGINE_THREAD_MSG_CONTINUE_PROCESS:
                String processDefinitionId = (String) msg.obj;
                mEngineThread.continueProcess(processDefinitionId);
                break;
            case ENGINE_THREAD_MSG_DELETE_PROCESS:
                String deploymentId = (String) msg.obj;
                mEngineThread.deletePreviousProcessDeployment(deploymentId);
                break;
            case ENGINE_THREAD_MSG_FINISH_TASK:
                String executionId = (String) msg.obj;
                mEngineThread.finishTask(executionId);
                break;
            case ENGINE_THREAD_MSG_CONTINUE_TASK:
                String continueExecutionId = (String) msg.obj;
                mEngineThread.continueTask(continueExecutionId);
                break;
            case ENGINE_CAPTURE_INSTANCE_STATE:
                String processInstanceId = (String) msg.obj;
                mEngineThread.emigrateProcess(processInstanceId);
                break;
            case EMIGRATE_PROCESS_INSTANCES:
                String[] processInstanceIds = (String[]) msg.obj;
                mEngineThread.emigrateProcesses(processInstanceIds);
                break;
            case ENGINE_LOAD_STATE_TO_DB:
                String fileName = (String) msg.obj;
                mEngineThread.immigrateProcess(fileName);
                break;
        }
    }
}
