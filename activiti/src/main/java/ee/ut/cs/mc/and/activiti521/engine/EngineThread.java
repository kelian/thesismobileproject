package ee.ut.cs.mc.and.activiti521.engine;

import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.Job;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ee.ut.cs.mc.and.activiti521.ExperimentUtils;
import ee.ut.cs.mc.and.activiti521.engine.migration.MigrationListener;
import ee.ut.cs.mc.and.activiti521.engine.migration.Migrator;
import ee.ut.cs.se.mt.activiti.ActivitiApp;
import ee.ut.cs.se.mt.activiti.model.Plan;

import static ee.ut.cs.mc.and.activiti521.ExperimentUtils.experimentLog;

public class EngineThread extends HandlerThread {
    private static final String TAG = EngineThread.class.getSimpleName();

    private static final String driver = "org.sqldroid.SQLDroidDriver";

    private static boolean isTableCreated = true;

    private File dbFile;
    private String url;
    private Connection con;

    private ProcessEngine processEngine;
    private RepositoryService repositoryService;
    private RuntimeService runtimeService;
    private Migrator migrator;

    private Handler handler;

    public EngineThread() {
        super(TAG);
        start();
    }

    @Override
    public synchronized void start() {
        super.start();
        handler = new EngineThreadHandler(getLooper(), this);
        handler.post(new Runnable() {
            @Override
            public void run() {
                startEngine();
            }
        });
    }

    private void startEngine() {
        connectToDb();

        setupProcessEngine();
        setupRuntimeService();
    }

    private void setupProcessEngine() {
        ProcessEngines.init();
        processEngine = ProcessEngineConfiguration.createStandaloneProcessEngineConfiguration()
                .setDatabaseSchemaUpdate(getDatabaseSchemaUpdate())
                .setJdbcDriver(driver)
                .setJdbcUrl(url)
                .setCreateDiagramOnDeploy(false)
                .setJobExecutorActivate(true)
                .setAsyncExecutorActivate(true)
                .setAsyncExecutorEnabled(true)
                .buildProcessEngine();

        Log.i(TAG, "Process Engine built");

        repositoryService = processEngine.getRepositoryService();
    }

    private void setupRuntimeService() {
        runtimeService = processEngine.getRuntimeService();
        runtimeService.addEventListener(new MigrationListener(handler));
    }

    private String getDatabaseSchemaUpdate() {
        if (isTableCreated) {
            return ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE;
        } else {
            return ProcessEngineConfiguration.DB_SCHEMA_UPDATE_CREATE_DROP;
        }
    }

    private void connectToDb() {
        try {
            if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                Log.e(TAG, "No SD Card");
            } else {
                File dir = ActivitiApp.getAppContext().getExternalFilesDir("Activiti");
                System.out.println("DIR: " + dir.getAbsolutePath());

                //create folder
                File file = new File(dir, "database");
                boolean wasFolderCreated = file.mkdirs();

                if (wasFolderCreated) {
                    isTableCreated = false;
                }

                //create file
                dbFile = new File(dir, "main.sqlite");

                url = "jdbc:sqlite:/" + dbFile.getPath();

                System.out.println(getClass().getCanonicalName() + ": SQLite path: " + url);
                // Class.forName(driver).newInstance();
//                if (dbFile.exists()) {
//                    dbFile.delete();
//                }
                con = DriverManager.getConnection(url);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void immigrateProcess(String fileName) {
        if (migrator == null) {
            migrator = new Migrator(con);
        }
        Log.i(TAG, "Reading .json and storing to DB...");
        migrator.loadStateFromFileToDb(fileName);

        experimentLog("Activating loaded instance");
        processEngine.getRuntimeService().activateProcessInstanceById(
                ExperimentUtils.IMMIGRATION_PROC_INST_ID);
        experimentLog("Immigration finished");
    }

    protected void emigrateProcess(String processInstanceId) {
        if (migrator == null) {
            migrator = new Migrator(con);
        }
        migrator.emigrateProcess(processInstanceId);
        ExperimentUtils.finishedMigration(processInstanceId);
    }

    protected void emigrateProcesses(String[] processInstanceIds) {
        if (migrator == null) {
            migrator = new Migrator(con);
        }
        migrator.emigrateProcessList(processInstanceIds);
        ExperimentUtils.finishedMigration(processInstanceIds);
    }

    public void startProcess(String processKey) {
        Log.i(TAG, "Starting Process Instance using key:" + processKey + " Thread ID=" + Thread.currentThread().getId());
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("employeeName", "Kermit");
        variables.put("numberOfDays", new Integer(4));
        variables.put("vacationMotivation", "I'm really tired!");
//        processEngine.getProcessEngineConfiguration().getJobExecutor().start();
        runtimeService.startProcessInstanceByKey(processKey, variables);
    }

    public void deployProcess(Plan plan) {
        repositoryService.createDeployment()
                .addString(plan.getBpmnResourceName(), plan.getBpmnFile())
                // .addClasspathResource(classPathResource)
                .deploy();
        Log.i(TAG, "Process Deployed! name = " + repositoryService.createProcessDefinitionQuery().list().get(0).getName());
    }

    public void deletePreviousProcessDeployment(String deploymentId) {
        repositoryService.deleteDeployment(deploymentId);
    }

    public void continueProcess(String processInstanceId) {
        List<Execution> executions = processEngine.getRuntimeService().createExecutionQuery().processInstanceId(processInstanceId).list();
        Log.i(TAG, "Attempting to continue " + executions.get(0));
        continueTask(executions.get(0).getId());
    }

    public void finishTask(String executionId) {
        Log.i(TAG, "EngineThread sending signals to execution " + executionId);
        runtimeService.signal(executionId);
//        runtimeService.signalEventReceived("finish", executionId); // works only if process subscribes to event with given name (e.g. in process definition)
    }

    public void continueTask(String executionId) {
        List<Job> jobList = processEngine.getManagementService().createJobQuery().executionId(executionId).list();
        if (!jobList.isEmpty()) {
            processEngine.getManagementService().executeJob(jobList.get(0).getId());
        } else {
            System.out.println("job list is empty");
        }
    }

    public Handler getHandler() {
        return handler;
    }

    public Connection getConnection() {
        return con;
    }

    public ProcessEngine getProcessEngine() {
        return processEngine;
    }

    public boolean isEngineInitialized() {
        return processEngine != null;
    }

    public EngineThread getInstance() {
        return this;
    }
}
