package ee.ut.cs.mc.and.activiti521.engine;

import ee.ut.cs.se.mt.activiti.model.Plan;

/**
 * Created by J-Kool on 30/09/2016.
 */
public interface EngineInterface {

    void startProcess(String processId);

    void deployProcess(Plan plan);
    void getEngineStatus(RunnableListener callback);

    interface RunnableListener<T> {
        void onResult(T arg);
    }

}
