# Master Thesis Mobile Application

The application runs the BPMN plans, which are received from the framework's backend. The application has two application templates implemented - hello world and tire change.

## 1. Architecture

The application consists of two modules:

* activiti - contains Activiti process engine and the networking layer for the backend.
* app - contains UI classes for the mobile application

## 2. Getting started

1. Set up the backend project - https://bitbucket.org/kelian/thesismobilebeproject/src/master/
2. Checkout the source code.
3. Modify the class `activiti/main/java/ee/ut/cs/se/mt/activiti/network/WebServiceClient.java` and add your IP address
4. Build the project and run the app.

## 3. Adding new Application Template
1. Create the new Application Template object for the database JSON.
2. Create custom JavaDelegate class and add it to the project.
3. Create the custom Activity class used in the JavaDelegate class and add it to the project.
4. Create Activiti BPMN snippet and set the service task path to JavaDelegate file path.
5. Add the Application Template to the database JSON in backend project.

### 3.1. Creating Java Delegate class

1. The class needs to extend `TaskActivitiDelegate`.
2. Override the method `setActivityClass` and add the used Activity class.

```
override fun setActivityClass() {
    activityClass = CreatedActivity::class.java // Add the used Activity class here
}
```

### 3.2. Creating Android Activity class used in Java Delegate

1. The class needs to extend `TaskActivitiActivity`.
2. The class needs to have a button which sets `finishListener` as OnClickListener. The user can use the button to finish the task.
3. The class XML layout file needs to start with `ConstraintLayout`.

## 4. Running the new Application Template
1. Create Generate Plan Activity that handles creating a new BPMN plan.
2. Create App Activity that starts the process engine and the created plan.

### 4.1. Create Generate Plan Activity
1. Create a new Activity that extends `GeneratePlanActivity`.
2. The activity must call a method `generatePlan(..)` that takes in three parameters.
    * View - the id of the layout file's `ConstraintLayout`.
    * PlanRequest
        * appId - Application Template's id in the database JSON file
        * requirements - list of user specified requirements
        * metric - optional, can be null.
    * Network Listener - implement the interface which has two methods:
        * `onGeneratePlanSuccess(Plan plan)` - is called when API request is successful. Returns the generated BPMN plan.
        * `onGeneratePlanFailure()` - is called when API request is unsuccessful.

### 4.2. Create App Activity
1. Create a new Activity that extends `AppActivitiActivity`.
2. Call method `deployProcess(Plan plan)` which deploys the generated BPMN plan.
3. In `onCreate` call method `initEngine(..)` that takes in two parameters:
    * EngineStatusListener - implement the interface which has one method:
        * `isProcessDeployed(ProcessDefinition process)` - responds with the deployed process.
    * appName - the name of the Application Template from the database JSON file
4. To start the plan, call method `startProcess(String processKey)` which starts the BPMN process.
    * The `processKey` is accessible from `ProcessDefinition`.

## 5. References

### 5.1. Images & Text

1. https://illlustrations.co/
2. https://lottiefiles.com/1055-world-locations
3. https://lottiefiles.com/18123-developer
4. https://www.wikihow.com/Change-a-Tire
5. michelinman.com/tirePressure.html

### 5.2. BeeWi BBW200 - Smart Temperature & Humidity Sensor

The implementation of `TirePressureBluetoothActivity` is from https://github.com/enrimilan/BeeWi-BBW200-Reader/
