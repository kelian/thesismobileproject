package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class RemoveTheNutsOtherTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = RemoveTheNutsOtherActivity::class.java
    }
}
