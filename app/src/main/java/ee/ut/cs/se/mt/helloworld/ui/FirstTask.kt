package ee.ut.cs.se.mt.helloworld.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class FirstTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = FirstActivity::class.java
    }
}
