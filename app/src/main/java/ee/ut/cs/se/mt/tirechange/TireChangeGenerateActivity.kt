package ee.ut.cs.se.mt.tirechange

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.google.android.material.snackbar.Snackbar
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.GeneratePlanActivity
import ee.ut.cs.se.mt.activiti.model.Plan
import ee.ut.cs.se.mt.activiti.model.PlanRequest
import ee.ut.cs.se.mt.tirechange.TireChangeActivity.Companion.APP_NAME
import ee.ut.cs.se.mt.tirechange.TireChangeActivity.Companion.EXTRA_PROCESS_PLAN
import kotlinx.android.synthetic.main.activity_tire_change.toolbar
import kotlinx.android.synthetic.main.activity_tire_change_generate.*

class TireChangeGenerateActivity : GeneratePlanActivity() {

    companion object {
        private const val REQUEST_ENABLE_BT = 1

        fun getIntent(context: Context) =
                Intent(context, TireChangeGenerateActivity::class.java)
    }

    private val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_generate)

        initToolbar()
        initListeners()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            title = "Generate new plan"
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun initListeners() {
        button_allow_bluetooth.setOnClickListener {
            onAllowBluetoothClick()
        }
        button_generate.setOnClickListener {
            onGenerateButtonClick()
        }
    }

    private fun onGenerateButtonClick() {
        showLoader()
        generatePlan(constraint_view, getPlanRequest(), object : NetworkListener {
            override fun onGeneratePlanSuccess(plan: Plan) {
                onPlanGenerated(plan)
            }

            override fun onGeneratePlanFailure() {
                hideLoader()
            }
        })
    }

    private fun getPlanRequest(): PlanRequest {
        return PlanRequest(APP_NAME, getRequirements(), getMetric())
    }

    private fun getRequirements() : List<String> {
        val requirements = arrayListOf<String>()

        if (button_allow_bluetooth.isChecked) {
            requirements.add("has_bluetooth")
        }

        getCarType()?.let { requirements.add(it) }

        return requirements
    }

    private fun getMetric(): String {
        return when (button_group_metric.checkedButtonId) {
            button_metric_battery_saving.id -> "min_cost"
            else -> "min_duration"
        }
    }

    private fun getCarType(): String? {
        return when (button_group_car.checkedButtonId) {
            button_car_modern.id -> "has_modern_unibody"
            else -> "has_other_car"
        }
    }

    private fun onAllowBluetoothClick() {
        if (button_allow_bluetooth.isChecked) {
            setupBluetooth()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                button_allow_bluetooth.isChecked = false
            }
        }
    }

    private fun setupBluetooth() {
        bluetoothAdapter?.let {
            if (!bluetoothAdapter.isEnabled) {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
            }
        } ?: showDeviceNotSupportBluetooth()
    }

    private fun showDeviceNotSupportBluetooth() {
        button_allow_bluetooth.isChecked = false
        Snackbar.make(constraint_view, "Bluetooth is not supported", Snackbar.LENGTH_LONG).show()
    }

    private fun showLoader() {
        loader.visibility = View.VISIBLE
        button_generate.isEnabled = false
    }

    private fun hideLoader() {
        loader.visibility = View.GONE
        button_generate.isEnabled = true
    }

    private fun onPlanGenerated(plan: Plan) {
        val intent = Intent().apply {
            putExtra(EXTRA_PROCESS_PLAN, plan)
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
