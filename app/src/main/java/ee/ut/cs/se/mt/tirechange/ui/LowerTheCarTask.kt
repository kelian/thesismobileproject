package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class LowerTheCarTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = LowerTheCarActivity::class.java
    }
}
