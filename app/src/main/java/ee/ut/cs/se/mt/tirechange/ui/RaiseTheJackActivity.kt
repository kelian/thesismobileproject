package ee.ut.cs.se.mt.tirechange.ui

import android.os.Bundle
import com.squareup.picasso.Picasso
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_tire_change_text_image.*

class RaiseTheJackActivity : TaskActivitiActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_text_image)

        tire_text_headline.text = "Take out the spare tire and the jack."
        tire_text_body.text = getBodyText()

        Picasso.get().load("https://www.dummies.com/wp-content/uploads/414051.image4.jpg")
                .into(tire_image)

        button_done.setOnClickListener(finishListener)
    }

    private fun getBodyText() : String {
        var bodyText = ""
        bodyText += "Place the jack under the frame near the tire that you are going to change. Ensure that the jack is in contact with the metal portion of your car's frame \n\n"
        bodyText += "For most modern uni-body cars, there is a small notch or mark just behind the front wheel wells, or in front of the rear wheel wells where the jack is intended to be placed."
        return bodyText
    }
}
