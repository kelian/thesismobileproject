package ee.ut.cs.se.mt.tirechange.ui

import android.os.Bundle
import com.squareup.picasso.Picasso
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_tire_change_text_image.*

class LowerTheCarActivity : TaskActivitiActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_text_image)

        tire_text_headline.text = "Lower the car without applying full weight on the tire."
        tire_text_body.text = getBodyText()

        Picasso.get().load("https://www.wikihow.com/images/thumb/d/de/Change-a-Tire-Step-14-Version-9.jpg/aid2856-v4-728px-Change-a-Tire-Step-14-Version-9.jpg.webp")
                .into(tire_image)

        button_done.setOnClickListener(finishListener)
    }

    private fun getBodyText() : String {
        var bodyText = ""
        bodyText += "Tighten the nuts as much as possible.\n\n"
        bodyText += "Lower the car to the ground fully and remove the jack.\n\n"
        bodyText += "Finish tightening the nuts and replace the hubcap."
        return bodyText
    }
}
