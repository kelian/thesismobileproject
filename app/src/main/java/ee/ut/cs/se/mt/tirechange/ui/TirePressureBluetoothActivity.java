package ee.ut.cs.se.mt.tirechange.ui;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.UUID;

import ee.ut.cs.mc.and.activiti521.R;
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity;

public class TirePressureBluetoothActivity extends TaskActivitiActivity {

    private static final int REQUEST_ENABLE_BT = 1;
    private BluetoothAdapter btAdapter;

    private TextView temperatureTextView;
    private TextView humidityTextView;
    private TextView batteryTextView;
    private TextView errorTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tire_pressure_bluetooth);

        temperatureTextView = findViewById(R.id.tire_text_temperature);
        humidityTextView = findViewById(R.id.tire_text_humidity);
        batteryTextView = findViewById(R.id.tire_text_battery);
        errorTextView = findViewById(R.id.tire_text_error);

        Button doneButton = findViewById(R.id.button_done);
        doneButton.setOnClickListener(finishListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestBluetoothAndProceed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                new RetrieveDataTask().execute();
            } else {
                showErrorMessage("Bluetooth must be enabled.");
            }
        }
    }

    private void requestBluetoothAndProceed() {
        BluetoothManager btManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        if(btManager == null) {
            showErrorMessage("BluetoothManager not available.");
            return;
        }
        btAdapter = btManager.getAdapter();
        if(btAdapter == null) {
            showErrorMessage("BluetoothAdapter not available.");
            return;
        }

        if (!btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent,REQUEST_ENABLE_BT);
        }
        else {
            new RetrieveDataTask().execute();
        }
    }

    private void showErrorMessage(String msg) {
        errorTextView.setText("Error: " + msg);
    }

    private class RetrieveDataTask extends AsyncTask<Void, Void, SensorData> {

        private String error;

        @Override
        protected SensorData doInBackground(Void... params) {
            try {
                ISensorReader sensorReader = new GattSensorReader(btAdapter, getApplicationContext());
                String[] rawData = sensorReader.readRawData();
                SensorData sensorData = parseSensorData(rawData);
                System.out.println("temperature: " + sensorData.getTemperature()+"°C");
                System.out.println("humidity: " + sensorData.getHumidity() + "%");
                System.out.println("battery: " + sensorData.getBattery() + "%");
                return sensorData;
            }
            catch (RuntimeException e) {
                error = e.getMessage();
                return null;
            }

        }

        @Override
        protected void onPostExecute(SensorData sensorData) {
            if(sensorData == null) {
                showErrorMessage(error);
                return;
            }

            temperatureTextView.setText("Temperature: " + sensorData.getTemperature() + "°C");
            humidityTextView.setText("Humidity: " + sensorData.getHumidity() + "%");
            batteryTextView.setText("Battery: " + sensorData.getBattery() + "%");
        }

        private SensorData parseSensorData(String[] bytes) {

            double temperature = Integer.valueOf(bytes[2] + bytes[1], 16).shortValue() / 10.0;
            int humidity = Integer.parseInt(bytes[4], 16);
            int battery = Integer.parseInt(bytes[9], 16);

            SensorData sensorData = new SensorData();
            sensorData.setTemperature(temperature);
            sensorData.setHumidity(humidity);
            sensorData.setBattery(battery);
            return sensorData;
        }

    }

    public class SensorData {

        private double temperature;
        private int humidity;
        private int battery;

        public double getTemperature() {
            return temperature;
        }

        public void setTemperature(double temperature) {
            this.temperature = temperature;
        }

        public int getHumidity() {
            return humidity;
        }

        public void setHumidity(int humidity) {
            this.humidity = humidity;
        }

        public int getBattery() {
            return battery;
        }

        public void setBattery(int battery) {
            this.battery = battery;
        }

    }

    public interface ISensorReader {

        String[] readRawData();

    }

    public class GattSensorReader implements ISensorReader {

        private BluetoothAdapter bluetoothAdapter;
        private Context context;
        private String[] data = new String[10];
        private final Object synchObj = new Object();

        public GattSensorReader(BluetoothAdapter bluetoothAdapter, Context context) {
            this.bluetoothAdapter = bluetoothAdapter;
            this.context = context;
        }

        @Override
        public String[] readRawData() {
            String macAddress = "78:A5:04:5A:9E:3E";
            if(macAddress.isEmpty()) {
                throw new RuntimeException("Mac address missing.");
            }
            BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(macAddress);
            bluetoothDevice.connectGatt(context, false, new BeeWiSmartBtCallback(data, synchObj));
            synchronized (synchObj) {
                try {
                    // wait max 15 seconds for the callback to retrieve the data from the sensor
                    synchObj.wait(15000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if(data[0] == null) {
                throw new RuntimeException("Failed reading data.");
            }

            return data;
        }

    }

    public class BeeWiSmartBtCallback extends BluetoothGattCallback {

        private static final String SERVICE_UUID = "a8b3fa04-4834-4051-89d0-3de95cddd318";
        private static final String CHARACTERISTIC_UUID = "a8b3fb43-4834-4051-89d0-3de95cddd318";
        private String[] data;
        private final Object synchObj;

        public BeeWiSmartBtCallback(String[] data, Object synchObj) {
            this.data = data;
            this.synchObj = synchObj;
        }

        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
            gatt.discoverServices();
        }

        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
            BluetoothGattService service = gatt.getService(UUID.fromString(SERVICE_UUID));
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(CHARACTERISTIC_UUID));
            gatt.readCharacteristic(characteristic);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            System.out.println("raw data:");
            byte[] bytes = characteristic.getValue();
            for(int i=0; i<bytes.length; i++) {
                data[i] = String.format("%02X", bytes[i]);
                System.out.print(data[i] + " ");
            }
            System.out.println();
            gatt.disconnect();
            synchronized (synchObj) {
                synchObj.notify();
            }
        }

    }
}
