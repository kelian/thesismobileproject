package ee.ut.cs.se.mt.tirechange.ui

import android.net.Uri
import android.os.Bundle
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_tire_change_text_video.*

class PlaceSpareTireActivity : TaskActivitiActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_text_video)

        tire_text_headline.text = "Remove the tire and place the spare tire on the hub."
        tire_text_body.text = getBodyText()

        initVideoPlayer()

        button_done.setOnClickListener(finishListener)
    }

    private fun initVideoPlayer() {
        val uriPath = Uri.parse("https://www.wikihow.com/video/f/f7/Change%20a%20Tire%20Step%2010%20Version%208.360p.mp4")

        tire_video.setVideoURI(uriPath)
        tire_video.setOnCompletionListener {
            tire_video.start()
        }
        tire_video.requestFocus()
        tire_video.start()
    }

    private fun getBodyText() : String {
        var bodyText = ""
        bodyText += "Take care to align the rim of the spare tire with the wheel bolts, then put on the lug nuts.\n\n"
        bodyText += "Be sure to install the spare tire the correct way and not backwards. The valve stem of a doughnut tire should face outwards, away from the vehicle."
        return bodyText
    }
}
