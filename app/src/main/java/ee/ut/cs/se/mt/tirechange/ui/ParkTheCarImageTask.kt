package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class ParkTheCarImageTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = ParkTheCarImageActivity::class.java
    }
}
