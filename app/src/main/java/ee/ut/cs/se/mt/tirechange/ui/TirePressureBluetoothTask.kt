package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class TirePressureBluetoothTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = TirePressureBluetoothActivity::class.java
    }
}
