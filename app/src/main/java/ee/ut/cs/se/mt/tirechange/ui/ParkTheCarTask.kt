package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class ParkTheCarTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = ParkTheCarActivity::class.java
    }
}
