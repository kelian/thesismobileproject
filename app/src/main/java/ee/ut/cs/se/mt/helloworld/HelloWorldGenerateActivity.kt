package ee.ut.cs.se.mt.helloworld

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.GeneratePlanActivity
import ee.ut.cs.se.mt.activiti.model.Plan
import ee.ut.cs.se.mt.activiti.model.PlanRequest
import ee.ut.cs.se.mt.helloworld.HelloWorldActivity.Companion.APP_NAME
import ee.ut.cs.se.mt.helloworld.HelloWorldActivity.Companion.EXTRA_PROCESS_PLAN
import kotlinx.android.synthetic.main.activity_hello_world_generate.*

class HelloWorldGenerateActivity : GeneratePlanActivity() {

    companion object {
        fun getIntent(context: Context) =
                Intent(context, HelloWorldGenerateActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello_world_generate)

        initToolbar()

        button_generate.setOnClickListener {
            onGenerateButtonClick()
        }
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            title = "Generate new plan"
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun onGenerateButtonClick() {
        showLoader()
        generatePlan(constraint_view, getPlanRequest(), object : NetworkListener {
            override fun onGeneratePlanSuccess(plan: Plan) {
                onPlanGenerated(plan)
            }

            override fun onGeneratePlanFailure() {
                hideLoader()
            }
        })
    }

    private fun getPlanRequest(): PlanRequest {
        return PlanRequest(APP_NAME, arrayListOf())
    }

    private fun showLoader() {
        loader.visibility = View.VISIBLE
        button_generate.isEnabled = false
    }

    private fun hideLoader() {
        loader.visibility = View.GONE
        button_generate.isEnabled = true
    }

    private fun onPlanGenerated(plan: Plan) {
        val intent = Intent().apply {
            putExtra(EXTRA_PROCESS_PLAN, plan)
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
