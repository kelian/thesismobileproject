package ee.ut.cs.se.mt.tirechange.ui

import android.net.Uri
import android.os.Bundle
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_tire_change_text_video.*

class RemoveHubcapActivity : TaskActivitiActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_text_video)

        tire_text_headline.text = "Remove the hubcap and loosen the nuts by turning counterclockwise."
        tire_text_body.text = getBodyText()
        initVideoPlayer()

        button_done.setOnClickListener(finishListener)
    }

    private fun initVideoPlayer() {
        val uriPath = Uri.parse("https://www.wikihow.com/video/a/a1/Change%20a%20Tire%20Step%206%20Version%209.360p.mp4")

        tire_video.setVideoURI(uriPath)
        tire_video.setOnCompletionListener {
            tire_video.start()
        }
        tire_video.requestFocus()
        tire_video.start()
    }

    private fun getBodyText() : String {
        var bodyText = ""
        bodyText += "Don't take them all the way off; just break the resistance. By keeping the wheel on the ground when you first loosen the nuts, you'll make sure that you're turning the nuts instead of the wheel.\n\n"
        bodyText += "Use the wrench that came with your car or a standard cross wrench. Your wrench may have different sizes of openings on different ends. A correctly-sized wrench will slip easily over the nut, but will not rattle."
        return bodyText
    }
}
