package ee.ut.cs.se.mt.tirechange.ui

import android.os.Bundle
import com.squareup.picasso.Picasso
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_tire_change_text_image.*
class ParkTheCarImageActivity : TaskActivitiActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_text_image)

        tire_text_headline.text = "Find a flat, stable and safe place to change your tire."
        tire_text_body.text = getBodyText()

        Picasso.get().load("https://www.wikihow.com/images/thumb/3/3a/Change-a-Tire-Step-1-Version-9.jpg/aid2856-v4-728px-Change-a-Tire-Step-1-Version-9.jpg.webp")
                .into(tire_image)

        button_done.setOnClickListener(finishListener)
    }

    private fun getBodyText() : String {
        var bodyText = ""
        bodyText += "You should have a solid, level surface that will restrict the car from rolling. \n\n"
        bodyText += "Apply the parking brake and put car into \"Park\" position.\n\n"
        bodyText += "Place a heavy object (e.g., rock, concrete, spare wheel, etc.) in front of the front and back tires.\n\n"
        return bodyText
    }
}
