package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class PlaceSpareTireTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = PlaceSpareTireActivity::class.java
    }
}
