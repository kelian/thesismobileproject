package ee.ut.cs.se.mt.tirechange.ui

import android.os.Bundle
import com.squareup.picasso.Picasso
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_tire_change_text_image.*

class TightenTheNutsImageActivity : TaskActivitiActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_text_image)

        tire_text_headline.text = "Tighten the nuts by hand until they are all snug."
        tire_text_body.text = getBodyText()

        Picasso.get().load("https://s.driving-tests.org/wp-content/uploads/2012/02/jack.jpg")
                .into(tire_image)

        button_done.setOnClickListener(finishListener)
    }

    private fun getBodyText() : String {
        var bodyText = "They should turn easily at first.\n\n"
        bodyText += "Using the wrench, tighten the nuts as much as possible using a star pattern. To ensure the tire is balanced, don't completely tighten the nuts one at a time. Going in a star pattern around the tire, one nut across from another, give each nut a full turn until they are equally tight."
        return bodyText
    }
}
