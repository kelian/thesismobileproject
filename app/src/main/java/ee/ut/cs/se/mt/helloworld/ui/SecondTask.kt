package ee.ut.cs.se.mt.helloworld.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class SecondTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = SecondActivity::class.java
    }
}
