package ee.ut.cs.se.mt.tirechange.ui

import android.os.Bundle
import com.squareup.picasso.Picasso
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_tire_change_text_image.*

class PlaceSpareTireImageActivity : TaskActivitiActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_text_image)

        tire_text_headline.text = "Remove the tire and place the spare tire on the hub."
        tire_text_body.text = getBodyText()

        Picasso.get().load("https://s.driving-tests.org/wp-content/uploads/2012/02/spare-tire.jpg")
                .into(tire_image)
        button_done.setOnClickListener(finishListener)
    }

    private fun getBodyText() : String {
        var bodyText = ""
        bodyText += "Take care to align the rim of the spare tire with the wheel bolts, then put on the lug nuts.\n\n"
        bodyText += "Be sure to install the spare tire the correct way and not backwards. The valve stem of a doughnut tire should face outwards, away from the vehicle."
        return bodyText
    }
}
