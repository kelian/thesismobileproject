package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class RaiseTheJackTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = RaiseTheJackActivity::class.java
    }
}
