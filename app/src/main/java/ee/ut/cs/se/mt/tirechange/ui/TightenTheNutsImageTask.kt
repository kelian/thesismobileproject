package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class TightenTheNutsImageTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = TightenTheNutsImageActivity::class.java
    }
}
