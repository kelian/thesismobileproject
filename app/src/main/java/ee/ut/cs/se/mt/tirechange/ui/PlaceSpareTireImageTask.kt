package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class PlaceSpareTireImageTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = PlaceSpareTireImageActivity::class.java
    }
}
