package ee.ut.cs.se.mt

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.MainActivitiActivity
import ee.ut.cs.se.mt.activiti.MainActivitiActivity.EngineStatusListener
import ee.ut.cs.se.mt.helloworld.HelloWorldActivity
import ee.ut.cs.se.mt.tirechange.TireChangeActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_tire_change.toolbar
import org.activiti.engine.runtime.ProcessInstance

class MainActivity : MainActivitiActivity() {

    private var ongoingProcessInstanceId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        initToolbar()
        initListeners()
        initEngine(getEngineStatusListener())
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.app_name)
    }

    private fun initListeners() {
        button_hello_world.setOnClickListener { openHelloWorld() }
        button_tire_change.setOnClickListener { openTireChange() }

        button_continue_plan.setOnClickListener { continueProcess(ongoingProcessInstanceId) }
    }

    override fun onResume() {
        super.onResume()
        showLoader()
    }

    private fun getEngineStatusListener(): EngineStatusListener {
        return EngineStatusListener { processes ->
            if (processes.isEmpty()) {
                hideContinueOngoingPlan()
            } else {
                showContinueOngoingPlan(processes)
            }
            hideLoader()
        }
    }

    private fun showContinueOngoingPlan(processes: List<ProcessInstance>) {
        button_continue_plan.visibility = View.VISIBLE

        continue_new_plan_name.text = processes[0].processDefinitionName
        this.ongoingProcessInstanceId = processes[0].processInstanceId
    }

    private fun hideContinueOngoingPlan() {
        card_view_ongoing_plan.visibility = View.GONE
    }

    private fun hideLoader() {
        loader.visibility = View.GONE
    }

    private fun showLoader() {
        loader.visibility = View.VISIBLE
        card_view_ongoing_plan.visibility = View.VISIBLE
        button_continue_plan.visibility = View.GONE
    }

    private fun openHelloWorld() {
        startActivity(HelloWorldActivity.getIntent(this))
    }

    private fun openTireChange() {
        startActivity(TireChangeActivity.getIntent(this))
    }

}
