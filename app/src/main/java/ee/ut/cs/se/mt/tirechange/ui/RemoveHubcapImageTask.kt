package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class RemoveHubcapImageTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = RemoveHubcapImageActivity::class.java
    }
}
