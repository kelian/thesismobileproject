package ee.ut.cs.se.mt.tirechange.ui

import android.net.Uri
import android.os.Bundle
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_tire_change_text_video.*

class TightenTheNutsActivity : TaskActivitiActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_text_video)

        tire_text_headline.text = "Tighten the nuts by hand until they are all snug."
        tire_text_body.text = getBodyText()
        initVideoPlayer()

        button_done.setOnClickListener(finishListener)
    }

    private fun initVideoPlayer() {
        val uriPath = Uri.parse("https://www.wikihow.com/video/e/e9/Change%20a%20Tire%20Step%2011%20Version%209.360p.mp4")

        tire_video.setVideoURI(uriPath)
        tire_video.setOnCompletionListener {
            tire_video.start()
        }
        tire_video.requestFocus()
        tire_video.start()
    }

    private fun getBodyText() : String {
        var bodyText = "They should turn easily at first.\n\n"
        bodyText += "Using the wrench, tighten the nuts as much as possible using a star pattern. To ensure the tire is balanced, don't completely tighten the nuts one at a time. Going in a star pattern around the tire, one nut across from another, give each nut a full turn until they are equally tight."
        return bodyText
    }
}
