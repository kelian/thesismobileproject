package ee.ut.cs.se.mt.tirechange.ui

import android.os.Bundle
import com.squareup.picasso.Picasso
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_tire_change_text_image.*

class RemoveHubcapImageActivity : TaskActivitiActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_text_image)

        tire_text_headline.text = "Remove the hubcap and loosen the nuts by turning counterclockwise."
        tire_text_body.text = getBodyText()

        Picasso.get().load("https://www.wikihow.com/images/thumb/e/e8/Change-a-Hubcap-Step-2-Version-3.jpg/aid1153877-v4-728px-Change-a-Hubcap-Step-2-Version-3.jpg.webp")
                .into(tire_image)

        button_done.setOnClickListener(finishListener)
    }

    private fun getBodyText() : String {
        var bodyText = ""
        bodyText += "Don't take them all the way off; just break the resistance. By keeping the wheel on the ground when you first loosen the nuts, you'll make sure that you're turning the nuts instead of the wheel.\n\n"
        bodyText += "Use the wrench that came with your car or a standard cross wrench. Your wrench may have different sizes of openings on different ends. A correctly-sized wrench will slip easily over the nut, but will not rattle."
        return bodyText
    }
}
