package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class RaiseTheJackOtherTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = RaiseTheJackOtherActivity::class.java
    }
}
