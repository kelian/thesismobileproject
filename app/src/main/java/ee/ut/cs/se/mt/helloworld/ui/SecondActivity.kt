package ee.ut.cs.se.mt.helloworld.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_hello_second.*

class SecondActivity : TaskActivitiActivity() {

    companion object {
        fun getIntent(context: Context) =
                Intent(context, SecondActivity::class.java).setFlags(
                        Intent.FLAG_ACTIVITY_NEW_TASK
                )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello_second)

        button_done.setOnClickListener(finishListener);
    }
}
