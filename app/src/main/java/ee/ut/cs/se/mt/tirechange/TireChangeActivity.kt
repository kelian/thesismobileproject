package ee.ut.cs.se.mt.tirechange

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.AppActivitiActivity
import ee.ut.cs.se.mt.activiti.AppActivitiActivity.EngineStatusListener
import ee.ut.cs.se.mt.activiti.model.Plan
import kotlinx.android.synthetic.main.activity_tire_change.*
import org.activiti.engine.repository.ProcessDefinition

class TireChangeActivity : AppActivitiActivity() {

    companion object {
        const val APP_NAME = "tire_change"
        const val INSTANCE_GENERATE_CODE = 1
        const val EXTRA_PROCESS_PLAN = "extra_process_plan"

        fun getIntent(context: Context) =
                Intent(context, TireChangeActivity::class.java)
    }

    private var deployedProcessKey : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change)

        initToolbar()
        initEngine(getEngineStatusListener(), APP_NAME)
        initListeners()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.tire_change_name)
    }

    private fun initListeners() {
        button_generate_plan.setOnClickListener { openGenerateNewPlanPage() }
        button_start_new_plan.setOnClickListener { openNewPlan() }
    }

    private fun getEngineStatusListener() : EngineStatusListener {
        return EngineStatusListener { process ->
            if (process == null) {
                hideStartNewPlan()
            } else {
                showStartNewPlan(process)
            }
            hideLoader()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == INSTANCE_GENERATE_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                showLoader()

                val plan = data?.getSerializableExtra(EXTRA_PROCESS_PLAN) as Plan?
                plan?.let {
                    deployProcess(it)
                }
            }
        }
    }

    private fun showStartNewPlan(process: ProcessDefinition) {
        divider_start_new_plan.visibility = View.VISIBLE
        button_start_new_plan.visibility = View.VISIBLE

        start_new_plan_name.text = process.name
        deployedProcessKey = process.key
    }

    private fun hideStartNewPlan() {
        divider_start_new_plan.visibility = View.GONE
        button_start_new_plan.visibility = View.GONE
    }

    private fun hideLoader() {
        divider_loader.visibility = View.GONE
        loader.visibility = View.GONE
    }

    private fun showLoader() {
        divider_loader.visibility = View.VISIBLE
        loader.visibility = View.VISIBLE

        hideStartNewPlan()
    }

    private fun openNewPlan() {
        deployedProcessKey?.let { startProcess(it) }
    }

    private fun openGenerateNewPlanPage() {
        startActivityForResult(TireChangeGenerateActivity.getIntent(this), INSTANCE_GENERATE_CODE)
    }
}
