package ee.ut.cs.se.mt.tirechange.ui

import android.net.Uri
import android.os.Bundle
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_tire_change_text_video.*

class ParkTheCarActivity : TaskActivitiActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_text_video)

        tire_text_headline.text = "Find a flat, stable and safe place to change your tire."
        tire_text_body.text = getBodyText()

        initVideoPlayer()

        button_done.setOnClickListener(finishListener)
    }

    private fun initVideoPlayer() {
        val uriPath = Uri.parse("https://www.wikihow.com/video/d/df/Change%20a%20Tire%20Step%202%20Version%208.360p.mp4")

        tire_video.setVideoURI(uriPath)
        tire_video.setOnCompletionListener {
            tire_video.start()
        }
        tire_video.requestFocus()
        tire_video.start()
    }

    private fun getBodyText() : String {
        var bodyText = ""
        bodyText += "You should have a solid, level surface that will restrict the car from rolling. \n\n"
        bodyText += "Apply the parking brake and put car into \"Park\" position.\n\n"
        bodyText += "Place a heavy object (e.g., rock, concrete, spare wheel, etc.) in front of the front and back tires.\n\n"
        return bodyText
    }
}
