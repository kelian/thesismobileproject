package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class TirePressureManuallyTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = TirePressureManuallyActivity::class.java
    }
}
