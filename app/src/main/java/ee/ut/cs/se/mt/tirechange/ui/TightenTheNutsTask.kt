package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class TightenTheNutsTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = TightenTheNutsActivity::class.java
    }
}
