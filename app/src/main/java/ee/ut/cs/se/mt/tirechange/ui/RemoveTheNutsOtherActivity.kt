package ee.ut.cs.se.mt.tirechange.ui

import android.os.Bundle
import com.squareup.picasso.Picasso
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_tire_change_text_image.*
class RemoveTheNutsOtherActivity : TaskActivitiActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_text_image)

        tire_text_headline.text = "Pump or crank the jack to lift the tire off the ground. "
        tire_text_body.text = getBodyText()

        Picasso.get().load("https://www.dummies.com/wp-content/uploads/414049.image2.jpg")
                .into(tire_image)

        button_done.setOnClickListener(finishListener)
    }

    private fun getBodyText() : String {
        var bodyText = ""
        bodyText += "You need to lift it high enough to remove the flat tire and replace it with a spare.\n\n"
        bodyText += "Remove the nuts the rest of the way. Turn them counterclockwise until they are loose. Repeat with all lug nuts, then remove the nuts completely.\n\n"
        bodyText += "Although rare, some vehicles actually have reverse threaded lug nuts. These are usually much older cars from Chrysler and GM."
        return bodyText
    }
}
