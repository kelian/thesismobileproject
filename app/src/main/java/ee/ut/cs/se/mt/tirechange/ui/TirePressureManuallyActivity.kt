package ee.ut.cs.se.mt.tirechange.ui

import android.os.Bundle
import com.squareup.picasso.Picasso
import ee.ut.cs.mc.and.activiti521.R
import ee.ut.cs.se.mt.activiti.TaskActivitiActivity
import kotlinx.android.synthetic.main.activity_tire_change_text_image.*

class TirePressureManuallyActivity : TaskActivitiActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tire_change_text_image)

        tire_text_headline.text = "How to check tire pressure"
        tire_text_body.text = getBodyText()

        Picasso.get().load("https://di-uploads-pod8.dealerinspire.com/eliteacura/uploads/2018/07/Tire-Pressure-Gauge.jpg")
                .into(tire_image)

        button_done.setOnClickListener(finishListener)
    }

    private fun getBodyText() : String {
        var bodyText = "Insert tire pressure gauge into the valve stem on your tire.\n\n"
        bodyText += "Compare the measured psi to the psi found on the sticker inside the driver’s door of your vehicle or in owner’s manual. DO NOT compare to the psi on your tire’s sidewall.\n\n"
        bodyText += "If your psi is above the number, let air out until it matches. If below, add air until it reaches the proper number."
        return bodyText
    }
}
