package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class RemoveTheNutsTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = RemoveTheNutsActivity::class.java
    }
}
