package ee.ut.cs.se.mt.tirechange.ui

import ee.ut.cs.se.mt.activiti.TaskActivitiDelegate

class RemoveHubcapTask : TaskActivitiDelegate() {

    override fun setActivityClass() {
        activityClass = RemoveHubcapActivity::class.java
    }
}
